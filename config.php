<?php
// show errors before include tracy
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// mysql settings
const MYSQL_SERVER = 'localhost';
const MYSQL_LOGIN = 'root';
const MYSQL_PASSWD = 'root';
const MYSQL_DBNAME = 'skills-demonstration-2';
const MYSQL_CHARSET = 'utf8';

// include autoloader made by composer (better than require / include every one class separately)
if(file_exists(__DIR__. DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
	require_once __DIR__. DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
}
else {
	throw new Exception("Autoloader not found. Did you install all by 'composer install' ?");
}
