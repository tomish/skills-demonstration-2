## PLACHY TOMAS - SKILLS DEMONSTRATION

* gitlab :      https://gitlab.com/tomish/skills-demonstration-2
* linkedin :    https://www.linkedin.com/in/tomasplachy/

Welcome to my skills demonstration project. I have use just latte/latte engine for render templates and tracy for debugging

## REQUIREMENTS

check ``composer.json`` file, minimum requirements:
* PHP >= 7.1 (Best on LAMP)
* composer (https://getcomposer.org/), but it works stay alone, because all required packages are gitted

### INSTALL
1) install all packages of vendor by composer, run in console:

```bash
composer install
```

2) import data from ``db/import.sql``

if app not will works, try
```bash
composer dump-autoload -o
```
for actualise autoloader of all class in app

5) Run and enjoy

---------------------------------------
Preview:
![](preview.png)
