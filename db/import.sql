SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_typ_zanr` int(11) DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_39986E43DD90DB63` (`id_typ_zanr`),
  CONSTRAINT `FK_39986E43DD90DB63` FOREIGN KEY (`id_typ_zanr`) REFERENCES `typ_zanr` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `album`;
INSERT INTO `album` (`id`, `id_typ_zanr`, `nazev`, `created_at`, `updated_at`) VALUES
(1,	1,	'Simulation theory',	'2019-12-18 20:43:41',	'2019-12-18 20:43:41'),
(2,	2,	'Can`t stop the feeling',	'2019-12-18 20:45:34',	'2019-12-18 20:45:34'),
(3,	3,	'The best of Antonín Dvořák',	'2019-12-18 20:45:39',	'2019-12-18 20:45:39');

DROP TABLE IF EXISTS `album_interpret`;
CREATE TABLE `album_interpret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_album` int(11) DEFAULT NULL,
  `id_interpret` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E22768ED71D2F73E339C2E4` (`id_album`,`id_interpret`),
  KEY `IDX_E22768ED71D2F73` (`id_album`),
  KEY `IDX_E22768EE339C2E4` (`id_interpret`),
  CONSTRAINT `FK_E22768ED71D2F73` FOREIGN KEY (`id_album`) REFERENCES `album` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E22768EE339C2E4` FOREIGN KEY (`id_interpret`) REFERENCES `interpret` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `album_interpret`;
INSERT INTO `album_interpret` (`id`, `id_album`, `id_interpret`, `created_at`, `updated_at`) VALUES
(1,	1,	1,	'2019-12-18 20:46:16',	'2019-12-18 20:46:16'),
(2,	2,	2,	'2019-12-18 20:46:58',	'2019-12-18 20:46:58'),
(3,	3,	3,	'2019-12-18 20:47:02',	'2019-12-18 20:47:02');

DROP TABLE IF EXISTS `album_skladba`;
CREATE TABLE `album_skladba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_album` int(11) DEFAULT NULL,
  `id_skladba` int(11) DEFAULT NULL,
  `cislo_stopy` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8AF0867ED71D2F731BA721C5E4D3302` (`id_album`,`id_skladba`,`cislo_stopy`),
  KEY `IDX_8AF0867ED71D2F73` (`id_album`),
  KEY `IDX_8AF0867E1BA721C` (`id_skladba`),
  CONSTRAINT `FK_8AF0867E1BA721C` FOREIGN KEY (`id_skladba`) REFERENCES `skladba` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8AF0867ED71D2F73` FOREIGN KEY (`id_album`) REFERENCES `album` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `album_skladba`;
INSERT INTO `album_skladba` (`id`, `id_album`, `id_skladba`, `cislo_stopy`, `created_at`, `updated_at`) VALUES
(1,	1,	1,	10,	'2019-12-18 20:47:42',	'2019-12-18 20:47:42'),
(2,	2,	2,	1,	'2019-12-18 20:48:20',	'2019-12-18 20:48:20'),
(3,	3,	3,	1,	'2019-12-18 20:48:25',	'2019-12-18 20:48:25'),
(4,	1,	4,	6,	'0000-00-00 00:00:00',	NULL);

DROP TABLE IF EXISTS `doctrine_migrations`;
CREATE TABLE `doctrine_migrations` (
  `version` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `doctrine_migrations`;

DROP TABLE IF EXISTS `interpret`;
CREATE TABLE `interpret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_typ_narodnost` int(11) DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_630C7EC369C07188` (`id_typ_narodnost`),
  CONSTRAINT `FK_630C7EC369C07188` FOREIGN KEY (`id_typ_narodnost`) REFERENCES `typ_narodnost` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `interpret`;
INSERT INTO `interpret` (`id`, `id_typ_narodnost`, `nazev`, `created_at`, `updated_at`) VALUES
(1,	1,	'Muse',	'2019-12-18 20:03:25',	'2019-12-18 20:03:54'),
(2,	2,	'Justin Timberlake',	'2019-12-18 20:06:24',	'2019-12-18 20:06:24'),
(3,	3,	'Antonín Dvořák',	'2019-12-18 20:07:05',	'2019-12-18 20:07:05');

DROP TABLE IF EXISTS `skladba`;
CREATE TABLE `skladba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delka` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `skladba`;
INSERT INTO `skladba` (`id`, `nazev`, `delka`, `created_at`, `updated_at`) VALUES
(1,	'Dig Down',	'00:03:48',	'2019-12-18 20:23:12',	'2019-12-18 20:23:12'),
(2,	'Can`t stop the feeling',	'00:03:56',	'2019-12-18 20:24:43',	'2019-12-18 20:24:43'),
(3,	'From the New World Largo',	'00:09:55',	'2019-12-18 20:26:22',	'2019-12-18 20:26:22'),
(4,	'Something Human',	'00:03:46',	'2019-12-18 21:20:37',	'2019-12-18 21:20:37');

DROP TABLE IF EXISTS `typ_narodnost`;
CREATE TABLE `typ_narodnost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `typ_narodnost`;
INSERT INTO `typ_narodnost` (`id`, `nazev`, `created_at`, `updated_at`) VALUES
(1,	'United Kingdom',	'2019-12-18 19:58:55',	'2019-12-18 19:58:55'),
(2,	'United States of America',	'2019-12-18 19:59:45',	'2019-12-18 19:59:45'),
(3,	'Czech Republic',	'2019-12-18 20:00:58',	'2019-12-18 20:00:58');

DROP TABLE IF EXISTS `typ_zanr`;
CREATE TABLE `typ_zanr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `typ_zanr`;
INSERT INTO `typ_zanr` (`id`, `nazev`, `created_at`, `updated_at`) VALUES
(1,	'Rock',	'2019-12-18 20:01:16',	'2019-12-18 20:01:16'),
(2,	'Pop',	'2019-12-18 20:01:41',	'2019-12-18 20:01:41'),
(3,	'Classic',	'2019-12-18 20:02:47',	'2019-12-18 20:02:47');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_logged_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `user`;

-- 2019-12-19 20:18:25
