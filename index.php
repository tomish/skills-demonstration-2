<?php
/**
 * @author: Tomas Plachy <plachy.t@gmail.com>
 * created: 21. 12. 2019
 */

include_once  'config.php';

use Latte\Engine;
use Tracy\Debugger;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;
use App\UI\OwnHtmlElement\Components\Div\Div;
use App\UI\OwnHtmlElement\Components\Link\Link;
use App\UI\OwnHtmlElement\Components\Img\Img;
use App\UI\OwnHtmlElement\Components\Form\Form;
use App\UI\OwnHtmlElement\Components\Form\Components\Input\Input;
use App\UI\OwnHtmlElement\Components\Form\Components\Select\Select;
use App\Services\Database\Database\DatabaseService;

// enable tracy debugger, errors are logged to directory log
Debugger::enable(null, __DIR__ . DIRECTORY_SEPARATOR . 'log');

/**
 * Interface rootPresenterInterface
 */
interface rootPresenterInterface {

	public function render() : void;
}

/**
 * Class rootPresenter
 */
final class rootPresenter implements rootPresenterInterface {
	/** @var Div|null */
	private $rootDiv = null;

	/** @var  DatabaseService|null */
	private $databaseService = null;

	public function __construct()
	{
		$this->databaseService = new DatabaseService();

	}
	public function render() : void
	{


		header('Content-language: cs');
		header('Content-Type: text/html; charset=utf-8');

		$latte = new Engine();
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . 'header.latte' , []);
		echo '<h2>HTML elements rendered by own components</h2>';
		$stream = $this->beforeRender()->render();
		echo $stream;
		echo '<h2>Album with longest track</h2>';
		echo '<code>';
		echo $this->databaseService->listSqlQueryFindAlbumWithLongestTrackBySql();
		echo '</code>';
		Debugger::dump($this->databaseService->findAlbumWithLongestTrackBySql());
		echo '<h2>List of albums ordered by album name, album interpret, with count of tracks</h2>';
		echo '<code>';
		echo $this->databaseService->listSqlQueryFindAllBySQL();
		echo '</code>';
		Debugger::dump($this->databaseService->findAllBySQL());

		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . 'footer.latte' , []);
	}

	private function beforeRender() : OwnHtmlElementInterface
	{
		/** @var Div $div */
		$rootDiv = new Div();
		$rootDiv->setIdAttribute("root-own-html-element")
			->setClassAttribute('row');

		/** @var Div $childrenDiv */
		$childrenDiv = new Div();
		$childrenDiv->setIdAttribute('children-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>pure html content by setter method in generated div element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Div\Div</code></small><br>');

		$rootDiv->addChildrenElement($childrenDiv);

		/** @var Div $childrenDiv */
		$linkParentDiv = new Div();
		$linkParentDiv->setIdAttribute('link-parent-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>link element generated</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Link\Link</code></small><br>');

		/** @var Link $link */
		$link = new Link();
		$link->setHrefAttribute('http://plachytomas.cz')
			->setHtmlContent('<strong>plachy</strong>tomas.cz')
			->setTargetAttribute('_blank');

		$linkParentDiv->addChildrenElement($link);
		$rootDiv->addChildrenElement($linkParentDiv);

		for ($i = 0; $i < 3; $i++) {
			/** @var Div $childrenDiv */
			$imgParentDiv = new Div();
			$imgParentDiv->setIdAttribute('children-own-html-element')
				->setClassAttribute('col-xs-12 col-md-4')
				->setHtmlContent('<strong>Image element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Img\Img</code></small><br>');

			/** @var Img $img */
			$img = new Img();
			$img->setIdAttribute('some-img-id')
				->setClassAttribute('img-responsive')
				->setSrcAttribute('http://lorempixel.com/output/people-q-c-1920-1920-9.jpg')
				->setWidthAttribute(200)
				->setHeightAttribute(200)
				->setTitleAttribute('Titulek obrázku')
				->setAltAttribute('Alternativní název obrázku');

			$imgParentDiv->addChildrenElement($img);
			$rootDiv->addChildrenElement($imgParentDiv);
		}

		$formParentDiv = new Div();
		$formParentDiv->setIdAttribute('form-parent-own-html-element')
			->setClassAttribute('col-xs-12 col-sm-12 col-md-12 col-lg-12')
			->setHtmlContent('<strong>Form element</strong><br><small>by  <code>\App\UI\OwnHtmlElement\Components\Form\Form</code></small><br>');

		$formParentDiv->addChildrenElement($this->fillForm());
		$rootDiv->addChildrenElement($formParentDiv);

		$this->rootDiv = $rootDiv;
		return $this->rootDiv;
	}



	/**
	 * Better solution is in Nette/Forms. Its possible to use Checkbox and Radiobuttons lists
	 * @return OwnHtmlElementInterface
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	private function fillForm(): OwnHtmlElementInterface
	{
		// form
		/** @var Form $form */
		$form = new Form();
		$form->setActionAttribute('do-form.php')
			->setMethodAttribute('post');

		// form input
		/** @var Input $formInputText */
		$formInputText = new Input();

		$formItems = [];
		$formInputText->setTypeAttribute('text')
			->setNameAttribute('test-text-type')
			->setIdAttribute('form-test-text-type')
			->setClassAttribute('form-control')
			->setHtmlContent('Input type text');
		$formItems[] = $formInputText;

		/** @var Input $formInputRadio */
		$formInputRadio = new Input();
		$formInputRadio->setTypeAttribute('radio')
			->setNameAttribute('test-radio-type')
			->setIdAttribute('form-test-radio-type-1')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Select me')
			->setValueAttribute('option1');

		$formItems[] = $formInputRadio;

		/** @var Input $formInputRadio */
		$formInputRadio = new Input();
		$formInputRadio->setTypeAttribute('radio')
			->setNameAttribute('test-radio-type')
			->setIdAttribute('form-test-radio-type-2')
			->setClassAttribute('form-check-input')
			->setHtmlContent('No! Select me please')
			->setValueAttribute('option2');

		$formItems[] = $formInputRadio;

		/** @var Input $formInputCheckbox */
		$formInputCheckbox = new Input();
		$formInputCheckbox->setTypeAttribute('checkbox')
			->setNameAttribute('test-checkbox-type')
			->setIdAttribute('form-test-checkbox-type-1')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Check me')
			->setValueAttribute('option1');

		$formItems[] = $formInputCheckbox;

		/** @var Input $formInputCheckbox */
		$formInputCheckbox = new Input();
		$formInputCheckbox->setTypeAttribute('checkbox')
			->setNameAttribute('test-checkbox-type')
			->setIdAttribute('form-test-checkbox-type-2')
			->setClassAttribute('form-check-input')
			->setHtmlContent('Check me too')
			->setValueAttribute('option2');

		$formItems[] = $formInputCheckbox;

		$formSelect = new Select();
		$formSelect->setNameAttribute('form-test-select-type')
			->setNameAttribute('test-select-type')
			->setClassAttribute('form-control')
			->addOptionValue('pop', 'POP music')
			->addOptionValue('rock', 'ROCK music - selected value', false, true)
			->addOptionValue('classic', 'Classic music - disabled value', true);

		$formItems[] = $formSelect;

		/** @var Input $formInputSubmit */
		$formInputSubmit = new Input();
		$formInputSubmit->setTypeAttribute('submit')
			->setClassAttribute('btn btn-primary')
			->setValueAttribute('Odeslat formulář');

		foreach ($formItems as $formItem) {
			$form->addChildrenElement($formItem);
		}

		$form->addChildrenElement($formInputSubmit);

		return $form;
	}
}

// render page
$renderPage = new rootPresenter();
$renderPage->render();
