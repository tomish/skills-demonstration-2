<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement;

use Latte\Engine;

trait OwnHtmlElementTrait
{
	/**
	 * @var OwnHtmlElementInterface[]
	 */
	protected $childrenElements = [];

	/**
	 * @var OwnHtmlElementInterface|null
	 */
	protected $parentElement = null;

	/**
	 * @var string
	 */
	protected $classAttribute = "";

	/**
	 * @var string
	 */
	protected $idAttribute = "";

	/**
	 * @var string
	 */
	protected $styleAttribute = "";

	/**
	 * @inheritDoc
	 */
	public function addChildrenElement(OwnHtmlElementInterface $element): array
	{
		$this->childrenElements[] = $element;
		return $this->childrenElements;
	}

	/**
	 * @inheritDoc
	 */
	public function getChildrenElements(): array
	{
		return $this->childrenElements;
	}

	/**
	 * @inheritDoc
	 */
	public function getClassAttribute(): string
	{
		return $this->classAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function getIdAttribute(): string
	{
		return $this->idAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function getParentElement(): ?OwnHtmlElementInterface
	{
		return $this->parentElement;
	}

	/**
	 * @inheritDoc
	 */
	public function getStyleAttribute(): string
	{
		return $this->styleAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setClassAttribute(string $attribute)
	{
		$this->classAttribute= $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setIdAttribute(string $attribute)
	{
		$this->idAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setParentElement(?OwnHtmlElementInterface $element)
	{
		$this->parentElement = $element;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setStyleAttribute(string $attribute)
	{
		$this->styleAttribute = $attribute;
		return $this;
	}

}
