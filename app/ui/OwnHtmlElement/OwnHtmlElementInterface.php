<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement;

interface OwnHtmlElementInterface
{

	/**
	 * @param OwnHtmlElementInterface $element
	 *
	 * @return OwnHtmlElementInterface[]
	 */
	public function addChildrenElement(OwnHtmlElementInterface $element): array;

	/**
	 * @return OwnHtmlElementInterface[]
	 */
	public function getChildrenElements(): array;

	/**
	 * @return string
	 */
	public function getClassAttribute(): string;

	/**
	 * @return string
	 */
	public function getIdAttribute(): string;

	/**
	 * @return OwnHtmlElementInterface|null
	 */
	public function getParentElement(): ?OwnHtmlElementInterface;

	/**
	 * @return string
	 */
	public function getStyleAttribute(): string;

	public function render();

	/**
	 * @param string $attribute
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setClassAttribute(string $attribute);

	/**
	 * @param string $attribute
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setIdAttribute(string $attribute);

	/**
	 * @param OwnHtmlElementInterface|null $element
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setParentElement(?OwnHtmlElementInterface $element);

	/**
	 * @param string $attribute
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setStyleAttribute(string $attribute);
}
