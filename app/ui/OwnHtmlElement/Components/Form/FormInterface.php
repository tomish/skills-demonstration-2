<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form;

use App\UI\OwnHtmlElement\Attributes\TargetAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface FormInterface extends OwnHtmlElementInterface, TargetAbleInterface
{
	/**
	 * @return string
	 */
	public function getMethodAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return FormInterface
	 */
	public function setMethodAttribute(string $attribute) : FormInterface;

	/**
	 * @return string
	 */
	public function getActionAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return FormInterface
	 */
	public function setActionAttribute(string $attribute) : FormInterface;


}
