<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form;

use App\UI\OwnHtmlElement\Attributes\TargetAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Form implements FormInterface
{
	use OwnHtmlElementTrait;
	use TargetAbleTrait;

	/**
	 * @var string
	 */
	private $methodAttribute = "";

	/**
	 * @var string
	 */
	private $actionAttribute = "";

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}

	/**
	 * @inheritDoc
	 */
	public function getMethodAttribute(): string
	{
		return $this->methodAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setMethodAttribute(string $attribute): FormInterface
	{
		$this->methodAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getActionAttribute(): string
	{
		return $this->actionAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setActionAttribute(string $attribute): FormInterface
	{
		$this->actionAttribute = $attribute;
		return $this;
	}

}
