<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Input;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleInterface;
use App\UI\OwnHtmlElement\Attributes\NameAbleInterface;
use App\UI\OwnHtmlElement\Attributes\ValueAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface InputInterface extends OwnHtmlElementInterface, HtmlContentAbleInterface, NameAbleInterface, ValueAbleInterface
{
	/**
	 * @return string
	 */
	public function getTypeAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return OwnHtmlElementInterface
	 */
	public function setTypeAttribute(string $attribute);


}
