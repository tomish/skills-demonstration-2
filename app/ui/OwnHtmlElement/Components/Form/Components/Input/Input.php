<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Input;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\Attributes\NameAbleTrait;
use App\UI\OwnHtmlElement\Attributes\ValueAbleTrait;
use App\UI\OwnHtmlElement\Components\Exceptions\NotImplementedException;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Input implements InputInterface
{
	use OwnHtmlElementTrait;
	use HtmlContentAbleTrait;
	use NameAbleTrait;
	use ValueAbleTrait;

	const INPUT_TYPE_BUTTON = 'button';
	const INPUT_TYPE_SUBMIT = 'submit';
	const INPUT_TYPE_CHECKBOX = 'checkbox';
	const INPUT_TYPE_RADIO = 'radio';
	const INPUT_TYPE_FILE = 'file';
	const INPUT_TYPE_TEXT = 'text';
	const INPUT_TYPE_HIDDEN = 'hidden';
	const INPUT_TYPE_PASSWORD = 'password';
	const INPUT_TYPE_HTML5_COLOR = 'color';
	const INPUT_TYPE_HTML5_DATE = 'date';
	const INPUT_TYPE_HTML5_DATETIME_LOCAL = 'datetime-local';
	const INPUT_TYPE_HTML5_EMAIL = 'email';
	const INPUT_TYPE_HTML5_MONTH = 'month';
	const INPUT_TYPE_HTML5_NUMBER = 'number';
	const INPUT_TYPE_HTML5_RANGE = 'range';
	const INPUT_TYPE_HTML5_SEARCH = 'search';
	const INPUT_TYPE_HTML5_TEL = 'tel';
	const INPUT_TYPE_HTML5_TIME = 'time';
	const INPUT_TYPE_HTML5_URL = 'url';
	const INPUT_TYPE_HTML5_WEEK = 'week';

	/** @var string */
	private $typeAttribute = "";

	/**
	 * @inheritDoc
	 */
	public function getTypeAttribute(): string
	{
		return $this->typeAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setTypeAttribute(string $attribute)
	{
		$this->typeAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];

		switch ($this->getTypeAttribute()) {
			case self::INPUT_TYPE_TEXT :
			case self::INPUT_TYPE_HIDDEN :
			case self::INPUT_TYPE_PASSWORD :
				$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.general.latte',
					$parameters);
				break;
			case self::INPUT_TYPE_CHECKBOX:
			case self::INPUT_TYPE_RADIO:
			case self::INPUT_TYPE_FILE:
			case self::INPUT_TYPE_SUBMIT:
			case self::INPUT_TYPE_BUTTON:
				$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.' . $this->getTypeAttribute() . '.latte',
					$parameters);
				break;
			case self::INPUT_TYPE_HTML5_COLOR:
			case self::INPUT_TYPE_HTML5_DATE:
			case self::INPUT_TYPE_HTML5_DATETIME_LOCAL:
			case self::INPUT_TYPE_HTML5_EMAIL:
			case self::INPUT_TYPE_HTML5_MONTH:
			case self::INPUT_TYPE_HTML5_NUMBER:
			case self::INPUT_TYPE_HTML5_RANGE:
			case self::INPUT_TYPE_HTML5_SEARCH:
			case self::INPUT_TYPE_HTML5_TEL:
			case self::INPUT_TYPE_HTML5_TIME:
			case self::INPUT_TYPE_HTML5_URL:
			case self::INPUT_TYPE_HTML5_WEEK:
				throw new NotImplementedException();
				break;
			default:
				throw new NotImplementedException();
				break;
		}
	}
}
