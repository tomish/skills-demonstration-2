<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleInterface;
use App\UI\OwnHtmlElement\Attributes\ValueAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface OptionInterface extends OwnHtmlElementInterface, HtmlContentAbleInterface, ValueAbleInterface
{

	// todo: dodelat metody pro praci s option (disabled, selected)

	/**
	 * @return bool
	 */
	public function isSelectedAttribute() : bool;

	/**
	 * @param bool $attribute
	 *
	 * @return OptionInterface
	 */
	public function setSelectedAttribute(bool $attribute) : OptionInterface;

	/**
	 * @return bool
	 */
	public function isDisabledAttribute() : bool;

	/**
	 * @param bool $attribute
	 *
	 * @return OptionInterface
	 */
	public function setDisabledAttribute(bool $attribute) : OptionInterface;


}
