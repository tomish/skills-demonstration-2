<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

interface SelectFactoryInterface
{

	/**
	 * @return Select
	 */
	public function create();
}
