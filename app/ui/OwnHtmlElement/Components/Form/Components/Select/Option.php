<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\Attributes\ValueAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Option implements OptionInterface
{
	use OwnHtmlElementTrait;
	use HtmlContentAbleTrait;
	use ValueAbleTrait;

	/**
	 * @var bool
	 */
	private $disabledAttribute = false;

	/**
	 * @var bool
	 */
	private $selectedAttribute = false;

	/**
	 * @inheritDoc
	 */
	public function isDisabledAttribute(): bool
	{
		return $this->disabledAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setDisabledAttribute(bool $attribute): OptionInterface
	{
		$this->disabledAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function isSelectedAttribute(): bool
	{
		return $this->selectedAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setSelectedAttribute(bool $attribute): OptionInterface
	{
		$this->selectedAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}
}
