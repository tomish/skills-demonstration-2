<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\Attributes\NameAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;
use App\UI\OwnHtmlElement\Components\Form\Components\Select\Option;

class Select implements SelectInterface
{
	use OwnHtmlElementTrait;
	use HtmlContentAbleTrait;
	use NameAbleTrait;



	/**
	 * @var Option[]
	 */
	private $options = [];

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}


	/**
	 * @inheritDoc
	 */
	public function getAllOptions(): array
	{
		return $this->options;
	}

	/**
	 * @inheritDoc
	 */
	public function addOption(Option $option): SelectInterface
	{
		if(!$this->isOptionInOptions($option)) {
			$this->options[] = $option;
		}
		return $this;
	}

	private function isOptionInOptions(Option $optionForInsert) : bool
	{
		$isInOptions = false;
		/** @var Option $option */
		foreach ($this->options as $option) {
			$isInOptions = $option->getValueAttribute() ===  $optionForInsert->getValueAttribute() ? true : $isInOptions;
		}
		return $isInOptions;
	}
	/**
	 * @inheritDoc
	 */
	public function removeAllOptions(): SelectInterface
	{
		$this->options = [];
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function addOptionValue(string $value,
								   string $label,
								   bool $disabled = false,
								   bool $selected = false
	): SelectInterface
	{
		/** @var Option $option */
		$option = new Option();
		$option->setValueAttribute($value)
		->setHtmlContent($label)
			->setDisabledAttribute($disabled)
			->setSelectedAttribute($selected);

		$this->addOption($option);
		return $this;
	}
}
