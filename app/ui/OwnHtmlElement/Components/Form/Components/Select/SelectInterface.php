<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Form\Components\Select;

use App\UI\OwnHtmlElement\Attributes\NameAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface SelectInterface extends OwnHtmlElementInterface, NameAbleInterface
{
	/**
	 * @return Option[]
	 */
	public function getAllOptions() : array;

	/**
	 * @param Option $option
	 *
	 * @return SelectInterface
	 */
	public function addOption(Option $option) : SelectInterface;

	/**
	 * inserts option directly to select
	 * @param string $value
	 * @param string $label
	 * @param bool   $disabled
	 * @param bool   $selected
	 *
	 * @return SelectInterface
	 */
	public function addOptionValue(string $value, string $label, bool $disabled = false, bool $selected = false) : SelectInterface;
	/**
	 * @return SelectInterface
	 */
	public function removeAllOptions() : SelectInterface;

}
