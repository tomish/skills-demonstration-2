<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Div;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleInterface;
use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface DivInterface extends OwnHtmlElementInterface, HtmlContentAbleInterface
{

}
