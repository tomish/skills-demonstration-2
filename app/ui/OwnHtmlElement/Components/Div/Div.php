<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Div;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleInterface;
use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Div implements DivInterface
{
	use OwnHtmlElementTrait;
	use HtmlContentAbleTrait;
	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}


}
