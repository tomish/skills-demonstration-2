<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Img;

use App\UI\OwnHtmlElement\OwnHtmlElementInterface;

interface ImgInterface extends OwnHtmlElementInterface
{
	/**
	 * @return string
	 */
	public function getSrcAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return ImgInterface
	 */
	public function setSrcAttribute(string $attribute) : ImgInterface;

	/**
	 * @return string
	 */
	public function getAltAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return ImgInterface
	 */
	public function setAltAttribute(string $attribute) : ImgInterface;

	/**
	 * @return string
	 */
	public function getTitleAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return ImgInterface
	 */
	public function setTitleAttribute(string $attribute) : ImgInterface;

	/**
	 * @return int|null
	 */
	public function getWidthAttribute() : ?int;

	/**
	 * @param string $attribute
	 *
	 * @return ImgInterface
	 */
	public function setWidthAttribute(?int $attribute = null) : ImgInterface;

	/**
	 * @return int|null
	 */
	public function getHeightAttribute() : ?int;

	/**
	 * @param int|null $attribute
	 *
	 * @return ImgInterface
	 */
	public function setHeightAttribute(?int $attribute = null) : ImgInterface;
}
