<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Img;

use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Img implements ImgInterface
{
	use OwnHtmlElementTrait;

	/** @var  string */
	private $srcAttribute = "";

	/** @var  string */
	private $altAttribute = "";

	/** @var  string */
	private $titleAttribute = "";

	/** @var  int|null */
	private $widthAttribute = null;

	/** @var  int|null */
	private $heightAttribute = null;

	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}

	/**
	 * @inheritDoc
	 */
	public function getSrcAttribute(): string
	{
		return $this->srcAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setSrcAttribute(string $attribute): ImgInterface
	{
		$this->srcAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getAltAttribute(): string
	{
		return $this->altAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setAltAttribute(string $attribute): ImgInterface
	{
		$this->altAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getTitleAttribute(): string
	{
		return $this->titleAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setTitleAttribute(string $attribute): ImgInterface
	{
		$this->titleAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getWidthAttribute(): ?int
	{
		return $this->widthAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setWidthAttribute(?int $attribute = null): ImgInterface
	{
		$this->widthAttribute = $attribute;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getHeightAttribute(): ?int
	{
		return $this->heightAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setHeightAttribute(?int $attribute = null): ImgInterface
	{
		$this->heightAttribute = $attribute;
		return $this;
	}
}
