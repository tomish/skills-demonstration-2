<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Components\Link;

use App\UI\OwnHtmlElement\Attributes\HtmlContentAbleTrait;
use App\UI\OwnHtmlElement\Attributes\NameAbleTrait;
use App\UI\OwnHtmlElement\Attributes\TargetAbleTrait;
use App\UI\OwnHtmlElement\OwnHtmlElementTrait;
use Latte\Engine;

class Link implements LinkInterface
{
	use OwnHtmlElementTrait;
	use NameAbleTrait;
	use HtmlContentAbleTrait;
	use TargetAbleTrait;

	/**
	 * @var string
	 */
	private $hrefAttribute = "";
	/**
	 * @inheritDoc
	 */
	public function render()
	{
		$latte = new Engine();

		$parameters = [
			'element' => $this,
		];
		$latte->render(__DIR__ . DIRECTORY_SEPARATOR . (new \ReflectionClass($this))->getShortName() . '.latte', $parameters);
	}

	public function getHrefAttribute(): string
	{
		return $this->hrefAttribute;
	}

	public function setHrefAttribute(string $attribute): LinkInterface
	{
		$this->hrefAttribute = $attribute;
		return $this;
	}
}
