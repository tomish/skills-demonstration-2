<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

interface ValueAbleInterface
{
	/**
	 * @return string
	 */
	public function getValueAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return mixed
	 */
	public function setValueAttribute(string $attribute);
}
