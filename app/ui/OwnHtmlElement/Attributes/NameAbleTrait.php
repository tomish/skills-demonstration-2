<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

trait NameAbleTrait
{
	/** @var string  */
	protected $nameAttribute = "";


	public function getNameAttribute(): string
	{
		return $this->nameAttribute;
	}

	public function setNameAttribute(string $attribute)
	{
		$this->nameAttribute = $attribute;
		return $this;
	}
}
