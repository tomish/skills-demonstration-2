<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

trait ValueAbleTrait
{
	/** @var string  */
	protected $valueAttribute = "";


	public function getValueAttribute(): string
	{
		return $this->valueAttribute;
	}

	public function setValueAttribute(string $attribute)
	{
		$this->valueAttribute = $attribute;
		return $this;
	}
}
