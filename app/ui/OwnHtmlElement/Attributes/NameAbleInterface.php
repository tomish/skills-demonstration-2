<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

interface NameAbleInterface
{
	/**
	 * @return string
	 */
	public function getNameAttribute() : string;

	/**
	 * @param string $attribute
	 *
	 * @return mixed
	 */
	public function setNameAttribute(string $attribute);
}
