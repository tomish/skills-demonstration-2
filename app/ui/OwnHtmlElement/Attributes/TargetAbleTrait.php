<?php

/**
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 19.12.2019
 */
declare(strict_types=1);

namespace App\UI\OwnHtmlElement\Attributes;

trait TargetAbleTrait
{
	/**
	 * @var string
	 */
	private $targetAttribute = "";

	/**
	 * @inheritDoc
	 */
	public function getTargetAttribute(): string
	{
		return $this->targetAttribute;
	}

	/**
	 * @inheritDoc
	 */
	public function setTargetAttribute(string $attribute)
	{
		$this->targetAttribute = $attribute;
		return $this;
	}
}
