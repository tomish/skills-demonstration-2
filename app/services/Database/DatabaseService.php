<?php

/**
 * @author: Tomas Plachy <plachy@sovanet.cz>
 * created: 21. 12. 2019
 */

declare(strict_types=1);

namespace App\Services\Database\Database;

use mysqli;

class DatabaseService implements DatabaseServiceInterface
{

	/** @var mysqli|null  */
	private $conn = null;

	private function connect() : mysqli
	{
		$this->conn = new mysqli();
		$this->conn->connect(MYSQL_SERVER, MYSQL_LOGIN, MYSQL_PASSWD, MYSQL_DBNAME);
		$this->conn->set_charset(MYSQL_CHARSET);
		if ($this->conn->connect_error) {
			throw new CantConnectException("ERROR: Unable to connect: " . $this->conn->connect_error);
		}
		return $this->conn;

	}

	private function close() : void
	{
		if ($this->conn !== null) {
			$this->conn->close();
			$this->conn = null;
		}
	}

	/**
	 * @inheritDoc
	 */
	public function findAlbumWithLongestTrackBySql() : ?array
	{
		if($this->conn===null) {
			$this->connect();
		}

		$sql = "
		 SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, s.delka AS delka, s.nazev
		 FROM album a
		 INNER JOIN album_interpret ai on a.id = ai.id_album
		 INNER JOIN interpret i on ai.id_interpret = i.id
		 INNER JOIN album_skladba `as` on a.id = `as`.id_album
		 INNER JOIN skladba s on `as`.id_skladba = s.id
		 ORDER BY s.delka DESC LIMIT 1
		";

		/** @var \mysqli_result $result */
		$result = $this->conn->query($sql);

		$out = $result->fetch_all(MYSQLI_ASSOC);
		$result->close();
		$this->close();

		return $out;
	}

	/**
	 * @inheritDoc
	 */
	public function listSqlQueryFindAllBySql() : string
	{
		$sql = "
		SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, COUNT(albskl.id) as count_of_skladba
			FROM album a
			INNER JOIN album_interpret ai on a.id = ai.id_album
			INNER JOIN interpret i on ai.id_interpret = i.id
			INNER JOIN album_skladba albskl on a.id = albskl.id_album
			GROUP BY a.id
			ORDER BY interpret_nazev ASC, album_nazev ASC
		";
		return $sql;
	}

	public function listSqlQueryFindAlbumWithLongestTrackBySql() : string
	{
		$sql = "
		 SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, s.delka AS delka, s.nazev
		 FROM album a
		 INNER JOIN album_interpret ai on a.id = ai.id_album
		 INNER JOIN interpret i on ai.id_interpret = i.id
		 INNER JOIN album_skladba `as` on a.id = `as`.id_album
		 INNER JOIN skladba s on `as`.id_skladba = s.id
		 ORDER BY s.delka DESC LIMIT 1
		";

		return $sql;
	}
	/**
	 * @inheritDoc
	 */
	public function findAllBySQL() : ?array
	{
		if($this->conn===null) {
			$this->connect();
		}

		$sql = "
			SELECT a.nazev AS album_nazev, i.nazev AS interpret_nazev, COUNT(albskl.id) as count_of_skladba
			FROM album a
			INNER JOIN album_interpret ai on a.id = ai.id_album
			INNER JOIN interpret i on ai.id_interpret = i.id
			INNER JOIN album_skladba albskl on a.id = albskl.id_album
			GROUP BY a.id
			ORDER BY interpret_nazev ASC, album_nazev ASC

		";

		/** @var \mysqli_result $result */
		$result = $this->conn->query($sql);

		$out = $result->fetch_all(MYSQLI_ASSOC);
		$result->close();
		$this->close();

		return $out;
	}


}
