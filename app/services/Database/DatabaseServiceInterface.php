<?php
/**
 * @author: Tomas Plachy <plachy@sovanet.cz>
 * created: 21. 12. 2019
 */

declare(strict_types=1);

namespace App\Services\Database\Database;

interface DatabaseServiceInterface
{
	/**
	 * @return array|null
	 * @throws CantConnectException
	 */
	public function findAlbumWithLongestTrackBySql() : ?array;

	/**
	 * @return array|null
	 * @throws CantConnectException
	 */
	public function findAllBySQL() : ?array;

	/**
	 * @return string
	 */
	public function listSqlQueryFindAllBySql() : string;

	/**
	 * @return string
	 */
	public function listSqlQueryFindAlbumWithLongestTrackBySql() : string;
}
